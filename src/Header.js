import Title from './Title';
import ThemeSwitcher from './ThemeSwitcher';

function Header() {
  return (
    <header>
      <Title />
      <ThemeSwitcher />
    </header>
  );
}

export default Header;
